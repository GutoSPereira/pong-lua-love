require "math"

fps = 60

function love.load ( )
  -- equivalente ao "void setup()"

  love.window.setMode(800, 600) -- equivalente a "size(800,600)""
  windowWidth, windowHeight, flags = love.window.getMode( )

  padWidth, padHeight = 20, 80 -- largura e altura dos pads

  w, h = 20, 20 -- largura e altura da bolinha

  love.mouse.setVisible(false)

  init() -- função definida para iniciar todas as variáveis

  tela = 0 -- tela 0 é a inicial, porém apenas se o jogador ainda não tiver iniciado o jogo

  maxTimer = 200

  timer = 0

  countingUp = true

  mainFont = love.graphics.newFont(20)
  love.graphics.setFont(mainFont);

  love.graphics.setPointSize(4)

  love.graphics.setBlendMode("alpha","alphamultiply")

  -- assets
  institucional = love.graphics.newImage("assets/institucional.png")
  credits = love.graphics.newImage("assets/credits.png")

  wallHit = love.audio.newSource("assets/wall.mp3","static")
  enemyPoint = love.audio.newSource("assets/enemy-point.mp3","static")
  padHit = love.audio.newSource("assets/pad.mp3","static")

  youwin = love.audio.newSource("assets/you-win.mp3","static")
  youlose = love.audio.newSource("assets/you-lose.mp3","static")
  -- fim assets
end

function love.update ()
  tick = love.timer.getTime()-- referencia de tempo desde o inicio do "quadro"
  --[[
    Esta função faz com que as variáveis sejam automaticamente atualizadas a cada loop.
    Esta e a próxima função, love.draw(), representam, em conjunto, a função "void draw()"
  ]]
  if tela == 0 then
    if countingUp then
      timer = timer + 1
    else
      timer = timer - 1
    end

    if timer >= maxTimer then
      countingUp = false
    end

    if timer <= 0 and countingUp ~= true then
      timer = 0
      countingUp = true
      tela = 1
    end
  end

  if tela == 1 then
    if countingUp then
      timer = timer + 1
    else
      timer = timer - 1
    end

    if timer >= maxTimer then
      countingUp = false
    end

    if timer <= 0 and countingUp ~= true then
      timer = 0
      countingUp = true
      tela = 2
      love.graphics.setColor(255,255,255, 100)
    end
  end -- tela 1

  if (tela == 3) then
    -- tela de jogo
    xPosition = xPosition + (xVelocity * xVelocityMultiplier)
    yPosition = yPosition + (yVelocity * yVelocityMultiplier)

    -- colisões com a parede
    -- baixo
    if (yPosition + h >= windowHeight) then
      yPosition = windowHeight - h
      yVelocity = -yVelocity

      love.audio.play(wallHit)
    end
    -- cima
    if (yPosition <= 0) then
      yPosition = 0
      yVelocity = -yVelocity

      love.audio.play(wallHit)
    end
    -- direita
    if (xPosition + w >= windowWidth) then
      xPosition = windowWidth - w
      xVelocity = -xVelocity

      love.audio.play(enemyPoint)

      points = points - 1
    end
    -- esquerda
    if (xPosition <= 0) then
      xPosition = 0
      xVelocity = -xVelocity

      pointEnabler = true

      love.audio.play(wallHit)
    end
    -- fim colisões com a parede

    -- colisões com o pad do usuário
    if (xPosition + w >= (windowWidth - 20 - padWidth)) then
      if (xPosition <= (windowWidth - 20 - (padWidth / 2))) then
        if (yPosition + h >= mouseY - (padHeight / 2)) then
          if (yPosition <= mouseY + (padHeight / 2)) then

            if pointEnabler == true then
              points = points + 1
            end

            love.audio.play(padHit)
            
            xPosition = windowWidth - 20 - padWidth - w;
            xVelocity = -xVelocity

            pointEnabler = false
          end
        end
      end
    end
    -- fim colisões com o pad do usuário

    -- colisão com o "pad"(faux) do computador
    if (xPosition <= 20 + padWidth) then
      love.audio.play(padHit)
      
      xVelocity = -xVelocity

      pointEnabler = true
    end
    -- fim colisão com o "pad"(faux) do computador

    if (points >= 1) then
      level = level + 1

      xVelocity = -((xVelocity / math.abs(xVelocity)) * (math.abs(xVelocity) + 1))
      yVelocity = (yVelocity / math.abs(yVelocity)) * (math.abs(yVelocity) + 1)

      points = 0
    end

    if (points < 0) then
      tela = 5 -- tela de perdedor
      love.audio.play(youlose)
    end

    if level > 5 then
      tela = 4
      love.audio.play(youwin)
    end

    mouseX, mouseY = love.mouse.getPosition( )

    if mouseY <= padHeight / 2 then
      padYPosition = padHeight / 2
    elseif mouseY >= windowHeight - (padHeight / 2) then
      padYPosition = windowHeight - (padHeight / 2)
    else
      padYPosition = mouseY
    end

    if yPosition <= padHeight / 2 then
      enemyPadYPosition = padHeight / 2
    elseif yPosition >= windowHeight - (padHeight / 2) then
      enemyPadYPosition = windowHeight - (padHeight / 2)
    else
      enemyPadYPosition = yPosition
    end

  end
end

function love.draw()
  if tela == 0 then
    if (timer <= 100) then
      love.graphics.setColor(255,255,255, timer/100)
    end
    love.graphics.draw(institucional, 0, 0)
  end

  if tela == 1 then
    if (timer <= 100) then
      love.graphics.setColor(255,255,255, timer/100)
    end
    love.graphics.draw(credits, 0, 0)
  end

  if (tela == 2) then
    -- tela inicial
    local iniText = "Pressione \"ENTER\" para começar"
    text(iniText, windowWidth/2, windowHeight/2, "center", mainFont, nil)
  end

  if (tela == 3) then
    -- tela de jogo
    -- bolinha
    love.graphics.rectangle("fill", xPosition, yPosition, w, h)
    -- pad jogador
    love.graphics.rectangle("fill", windowWidth - 20 - padWidth, padYPosition - (padHeight / 2), padWidth, padHeight) 
    -- pad inimigo
    love.graphics.rectangle("fill", 20, enemyPadYPosition - (padHeight / 2), padWidth, padHeight); 
    
    text("Nível: " .. level, windowWidth/2, 25, "center", mainFont, nil)
    love.graphics.setColor(255,0,0,100)
    love.graphics.points(mouseX, mouseY)
    love.graphics.setColor(255,255,255,100)
  end

  if (tela == 4) then
    -- tela de vencedor
    local winText = "Você ganhou!\nPressione \"ENTER\" para jogar de novo\nPressione \"ESCAPE\" para sair"
    
    text(winText, windowWidth/2, windowHeight/2, "center", mainFont, nil)
  end

  if (tela == 5) then
    -- tela de perdedor
    local loseText = "Você perdeu...\nPressione \"ENTER\" para tentar de novo\nPressione \"ESCAPE\" para sair"
    text(loseText, windowWidth/2, windowHeight/2, "center", mainFont, nil)
  end

  tock = love.timer.getTime() - tick -- tempo desde o início do quadro(tick) até o final(tock)

  if (tock < (1 / 30)) then
    -- text(love.timer.getFPS() .. " " .. (1 / 30 - tock), windowWidth/2, windowHeight/2, "center", mainFont, nil)
    love.timer.sleep(1 / fps - tock)
  end
end

function love.keypressed(key)
  if ((key == "return" or key == "kpenter") and tela == 2) then
    tela = 3
  end
  if (tela == 4 or tela == 5) then
    if (key == "return" or key == "kpenter") then
      init()
      tela = 3
    end
    if (key == "escape") then
      love.event.quit()
    end
  end
end

function text(string, x, y, align, font, fontSize)
  if (align == nil) then
    align = "left"
  end

  if font == nil then
    if fontSize == nil then
      fontSize = 16
    end
    
    font = love.graphics.newFont(fontSize)
  end

  love.graphics.setFont(font)

  if align == "left" then
    love.graphics.print(string, x, y)
  end

  if align == "center" then
    love.graphics.print(string, x - (font:getWidth(string)/2), y - (font:getHeight(string) / 2))
  end

  if align == "right" then
    love.graphics.print(string, x - font:getWidth(string), y - font:getHeight(string))
  end
end

function init()
  points = 0

  xPosition = (windowWidth / 2) - (w / 2)
  yPosition = (windowHeight / 2) - (h / 2)

  xVelocity = 1
  yVelocity = 1

  yVelocityMultiplier = math.random(1,3.5)
  xVelocityMultiplier = math.random(1,3.5)

  level = 1

  pointEnabler = true
end
